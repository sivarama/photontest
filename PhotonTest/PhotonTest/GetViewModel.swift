//
//  GetViewModel.swift
//  PhotonTest
//
//  Created by Siva  Rama Ch on 2/15/24.
//

import Foundation


/* 
  I have created view model class in this. It is used to fetch data from the url.
 I have used ObservableObhect protocol to notify data if there is any change in the API response.
 
 */


class ViewModel: ObservableObject {
    @Published private(set) var getData = [SchoolDetails]()
    
    init() {
        Task.init {
            await fetchData()
        }
    }
    
    func fetchData() async {
        do {
            guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else { fatalError("Missing URL") }
            
            let urlRequest = URLRequest(url: url)
            
            let (data, response) = try await URLSession.shared.data(for: urlRequest)
            
            guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Error while fetching data") }
            
            let decoder = JSONDecoder()
           // decoder.keyDecodingStrategy = .convertFromSnakeCase
            let decodedData = try decoder.decode([SchoolDetails].self, from: data)
            
            DispatchQueue.main.async {
                self.getData = decodedData
            }
            
        } catch {
            print("Error fetching data from Pexels: \(error)")
        }
    }
}



