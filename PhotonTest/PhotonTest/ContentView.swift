//
//  ContentView.swift
//  PhotonTest
//
//  Created by Siva  Rama Ch on 2/15/24.
//

import SwiftUI

/*
 Created a State object of view model class.
 Passed view model data to list to populate to table.
 Inserted in Navigation stack to add heading to view.
 */

struct ContentView: View {
    
    @StateObject  var newYorkSchoolModel = ViewModel()

    
    var body: some View {
        
        NavigationStack {
            
            List(newYorkSchoolModel.getData) { eachSchool in
                Text(eachSchool.overview_paragraph)
            }
            .onAppear(perform: {
                if newYorkSchoolModel.getData.isEmpty {
                    Task {
                        await newYorkSchoolModel.fetchData()
                    }
                }
            })
            .navigationTitle("School list")
            
        }
    }
    
}

#Preview {
    ContentView()
}
