//
//  PhotonTestApp.swift
//  PhotonTest
//
//  Created by Siva  Rama Ch on 2/15/24.
//

import SwiftUI

@main
struct PhotonTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
