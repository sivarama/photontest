//
//  SchoolDetails.swift
//  PhotonTest
//
//  Created by Santosh Kumar Chantati on 2/15/24.
//

import Foundation

/*
 Model object to store and access the values.
Used Identifiable protocol to iterate through list.
 used codable protocol to parse data from API response.
 */
struct  SchoolDetails: Identifiable, Codable {
    let id = UUID().uuidString
    let dbn: String
    let school_name: String
    let overview_paragraph: String
}
